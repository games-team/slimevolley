#ifndef _CONFIG_H
#define _CONFIG_H 1

#cmakedefine DATA_DIR "${DATA_DIR}"
#cmakedefine APPLE
#cmakedefine WINDOWS
#cmakedefine I10N
#cmakedefine I10N_DIR "${I10N_DIR}"
#cmakedefine NET_SUPPORT
#cmakedefine SV_PORTABLE

#endif
